#include "SystemThreads.h"
#include "SystemConfig.h"
#include "hardware/ch6dm_linux.h"					// IMU Operations
#include "hardware/adcAccess.h"						// ADC Operations
//#include "tiC2000.h"						// Microcontroller Communication

/*******************************************************************************
* Function Name  : adcThreadRun
* Input          : SharedMemory Pointer
* Output         : Updates Shared Memory Variables (adcValues ...)
* Return         : None
* Description    : Reads adcDevice1 with an interval of adcSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/
void *adcThreadRun(void *param)
{
	//Shared Memory Instance
	void *securityPtr; char *shm;
	float currentRead;
	shm = (char*) (param);
    int shmid;
    key_t key;

	int argc = 1;
	char **argv = NULL;

	int fd, i, ch = -1;
	int avg = 0, machine_read = 0, opt;

	char *adcTestMode;

	if ( -1 == bindSharedMem(shm, key, securityPtr ) ){
		printf("SharedMemoryBinding Failed: adcThreadRun");
	}

	adcTestMode = (char*) getSM(shm, SM::adcTest);	//Read corresponding variables of AdcThread

	fd = open(adcDevice, O_RDWR | O_NONBLOCK, 0);
	if (fd < 0) {
		perror("open");
		printf("ADC Thread couldnt be opened\n");
		return NULL;
	}
	else{
		printf("ADC Thread has opened ADCport\n");

	while( 1 == 1 ) {
		for (i = MIN_OVERO_CHANNEL; i <= MAX_OVERO_CHANNEL; i++){
			currentRead = read_channel(fd, i, avg, machine_read, shm);	//Read ADC port
			currentRead = currentRead + 1 - 1;							//Filter if appropriate

			if ( *adcTestMode ){
				(i == MIN_OVERO_CHANNEL) ? printf("ADC CH2: %f\n", currentRead) : currentRead;
			}
			switch( i){
				//Write to SharedMem
				case 0: writeToSM(shm, SM::QR::ADC1, currentRead); break;
				case 1: writeToSM(shm, SM::QR::ADC2, currentRead); break;
				case 2: writeToSM(shm, SM::QR::ADC3, currentRead); break;
				case 3: writeToSM(shm, SM::QR::ADC4, currentRead); break;
				case 4: writeToSM(shm, SM::QR::ADC5, currentRead); break;
				case 5: writeToSM(shm, SM::QR::ADC6, currentRead); break;
				case 6: writeToSM(shm, SM::QR::ADC7, currentRead); break;
				case 7: writeToSM(shm, SM::QR::ADC8, currentRead); break;
				default: break;
			}
		}
		usleep(adcSleepPeriod);
	}

	close(fd);
	}


}

/*******************************************************************************
* Function Name  : serial1ThreadRun
* Input          : SharedMemory Pointer
* Output         : Updates Shared Memory Variables (imuRoll,imuPitch,imuYaw)
* Return         : None
* Description    : Reads serialDevice1 with an interval of serialSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/
void *serial1ThreadRun(void *param)
{

	//Shared Memory
	void *securityPtr; char *shm;
	shm = (char*) (param);
    int shmid; key_t key;

	//Local Variables
	float roll, pitch, yaw;				//Internal Storage
	float rollRate, pitchRate, yawRate; //Internal Storage
	char *serial1Test;					//Debugging

	if ( -1 == bindSharedMem(shm, key, securityPtr ) ){
		printf("SharedMemoryBinding Failed: serial1ThreadRun");
	}

	serial1Test = (char*) getSM(shm, SM::serial1Test);	//Read corresponding variables of AdcThread

	int fd;
	int numBytez;						//Number of Bytes read from serialInterface
	struct termios options;				//Define Serial Port Options

	if ( -1 == (fd = open_serial(serial1Device) )){	//Open Serial Port
		if ( *serial1Test )
		printf("Serial1 Thread cannot open port\n");
		return NULL;
	}
	else
		printf("Serial1 Thread has opened port\n");

	tcgetattr(fd, &options);			/*Configure Serial Port for Usage */
	cfsetispeed(&options, 115200);		/* Set the baud rates to 115200 */
	options.c_cflag |= (CLOCAL | CREAD);/* Enable the receiver and set local mode... */
	options.c_cflag |= CS8;				/* Select 8 data bits */

	options.c_cflag &= ~PARENB;			/* No parity (8N1) configuration */
	options.c_cflag &= ~CSTOPB;			/* No parity (8N1) configuration */
	options.c_cflag &= ~CSIZE;			/* No parity (8N1) configuration */
	options.c_cflag |= CS8;				/* No parity (8N1) configuration */
	tcsetattr(fd, TCSAFLUSH, &options); /* Apply changes immediately */

	unsigned char byte = 'o';
	unsigned char len;
	unsigned char dataBuffer[18];
	unsigned char flagByte1,flagByte2;

	while(1){
		search_header:
		while( byte != 's'){
			numBytez = read(fd, &byte, 1);
		}
		numBytez = read(fd, &byte, 1);
		if ( byte != 'n')
			goto search_header;
		numBytez = read(fd, &byte, 1);
		if ( byte != 'p')
			goto search_header;

		//Valid Package Start here
		if ( *serial1Test )
			printf("Valid Package Start Detected \n");

		numBytez = read(fd, &byte, 1);
		if ( byte != SENSOR_DATA){
			continue;
			/*Interpret ChannelData Here.
			 * v1.0 For now we Interpret Sensor Data Package
			 * v2.0 We will interpret more package types
			 */
		}

		numBytez = read(fd, &len, 1);
		numBytez = read(fd, &flagByte1, 1);
		numBytez = read(fd, &flagByte2, 1);
		/*Interpret ChannelData Here.
		 * v1.0 For now we get raw, pitch, roll with default settings
		 * v2.0 determine active channels and interpret all data
		 */

		if (*serial1Test) {
			printf("Type: %d\n", (unsigned int)byte);
			printf("Len: %d\n", (unsigned int)len);
			printf("Flag1: %d\n", (unsigned int)flagByte1);
			printf("Flag2: %d\n", (unsigned int)flagByte2);
		}

		/*Read all data to dataBuffer */
		int byteNum, readBytes = 0, requiredBytes = 6;
		while ( readBytes < requiredBytes){
			if ( -1 != ( byteNum = read(fd, &dataBuffer[readBytes], requiredBytes - readBytes) )){
				readBytes += byteNum;
			}

		}

        int act_channels = (flagByte1 << 8) | (flagByte2);
        int i = 0;
        int m_recentData[3];

        // Yaw angle
        if ((act_channels & 0x8000) != 0)
        {
            m_recentData[0] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            yaw = (float)m_recentData[0] * (.0109863);
            i += 2;
        }
        // Pitch angle
        if ((act_channels & 0x4000) != 0)
        {
        	m_recentData[1] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            pitch = (float)m_recentData[1] * (.0109863);
            i += 2;
        }

        // Roll angle
        if ((act_channels & 0x2000) != 0)
        {
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            roll = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

		//Flush Serial Buffer or we will process all packages by order...
		tcflush(fd, TCIOFLUSH );

		/***************SHARED MEMORY ACCESS ****************/
		if ( *serial1Test ){
			printf("yaw: %.2f pitch:%.2f roll:%.2f \n", yaw, pitch, roll);
		}

		writeToSM(shm, SM::QR::roll, roll);
		writeToSM(shm, SM::QR::pitch, pitch);
		writeToSM(shm, SM::QR::yaw, yaw);

		/***************SHARED MEMORY ACCESS ****************/

		usleep(serialSleepPeriod * 1000);		// first arg is in ms so...
		//system("clear");						//UNCOMMENT AFTER

	}

}



/*******************************************************************************
* Function Name  : sysStatusTXThreadRun
* Input          :
* Output         : Transmits system status to remote computer (Slow Comm)
* Return         : None
* Description    : Transmits system status with an interval of sysStatusSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/
void *sysStatusTXThreadRun(void *param)
{

}

void *sensorTXThreadRun(void *param)
{

}

/*******************************************************************************
* Function Name  : serial2ThreadRun
* Input          : SharedMemory Pointer
* Output         : Updates Shared Memory Variables (comm with microcontroller?)
* Return         : None
* Description    : Reads serialDevice1 with an interval of serialSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/
void *serial2ThreadRun(void *param)
{

}



void  *cameraThreadRun(void *param)
{

}

/*******************************************************************************
* Function Name  : loggerThreadRun
* Input          :
* Output         : Logs system status & Shared Memory to Blackbox
* Return         : None
* Description    :
*******************************************************************************/
void *loggerThreadRun(void *param)
{

}
