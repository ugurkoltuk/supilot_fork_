/*
 ============================================================================
 Name        : su_autopilot.h
 Author      : Taygun Kekec
 Version     : 1.0
 Date		 : March 2013
 Copyright   : GNU
 Description :
			   This file covers entry point of SuPilot application
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>			//Utility Library
#include <pthread.h>		//Posix Thread Library
#include <cmath>
#include <sys/types.h>
#include <sys/ipc.h>		//Unix Interprocess CommunicationLibrary
#include <sys/shm.h>		//Unix Shared Memory Library
#include <unistd.h>			//Parsing functionality
#include <ctype.h>

//#include "SystemConfig.h"
#include "SystemThreads.h"
#include "Utility.h"
#include "suShmLib.h"

/*System Thread Declarations */
pthread_t adcThread;				// copying ADC input to SharedMemory
pthread_t serial1Thread;			// copying Serial1 input to SharedMemory
pthread_t serial2Thread;			// copying Serial2 input to SharedMemory
pthread_t loggerThread;				// reading shared memory and writing states to logfile
pthread_t sysStatusTXThread;		// transmitting SystemStatus to remote	(MAVLINK)
pthread_t sensorTXThread;			// transmitting SharedMemory to remote	(MAVLINK)
pthread_t cameraThread;				// transmitting frames to the network

/* System Function Declarations */
void initSharedMemory();			// Initializes shared memory and its content
void initThreads();					// Initializes and starts threads
void main_loop();					// Main program loop
void checkSensorFailure();			// Periodically checks sensorFailures
void checkSystemFailure();			// Periodically checks systemFailures

char *shm;							// Shared Memory instance
int shmid;							// Shared Memory ID
key_t key;							// Shared Memory Key


void main_loop()
{


	// Comment out all printfs.
	printf("Shared Memory Initialized\n");
	for(;;){
		usleep(2500000);
		printf("Bara bara bere\n");


	}

}

void initThreads()
{
	int  iret1, iret2, iret3, iret4, iret5, iret6, iret7;
	/* Create independent threads each of which will execute function */
	iret1 = pthread_create( &adcThread, NULL, adcThreadRun, (void*) shm);
	iret2 = pthread_create( &serial1Thread, NULL, serial1ThreadRun, (void*) shm);
	/* Serial2 is TI Comm for now */
	//iret3 = pthread_create( &serial2Thread, NULL, serial2ThreadRun, (void*) shm);
	iret4 = pthread_create( &loggerThread, NULL, loggerThreadRun, (void*) shm);
	iret5 = pthread_create( &sysStatusTXThread, NULL, sysStatusTXThreadRun, (void*) shm);
	iret6 = pthread_create( &sensorTXThread, NULL, sensorTXThreadRun, (void*) shm);
	iret7 = pthread_create( &cameraThread, NULL, cameraThreadRun, (void*) shm);
}

void initSharedMemory()
{
	void *securityPtr;				// C++ does not support direct casting of char* to void*

    /* Get the segment named "5678", created by the server. */
    key = SHARED_MEMORY_KEY  ;

    /* Create the segment. */
    if ((shmid = shmget(key, SHARED_MEMORY_SIZE, IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    /* Now we attach the segment to our data space. */
    if ((securityPtr = shmat(shmid, NULL, 0)) == (char *) -1) {
        perror("shmat");
        exit(1);
    }
	shm = (char*) securityPtr;

	/* Initialize shared memory with SHARED_MEMORY_INIT_VAL to let client error check. */
	memset (shm, SHARED_MEMORY_INIT_VAL ,SHARED_MEMORY_SIZE);

	//Initialize IMU data
	writeToSM( shm, SM::QR::roll , 10.20f);
	writeToSM( shm, SM::QR::pitch , -5.22f);
	writeToSM( shm, SM::QR::yaw , 12.87f);
	//Initialize Virtual Control data
	writeToSM( shm, SM::QR::U1 , 0);
	writeToSM( shm, SM::QR::U2 , 0);
	writeToSM( shm, SM::QR::U3 , 0);
	writeToSM( shm, SM::QR::U4 , 0);
	//Initialize Motor Duties
	writeToSM( shm, SM::QR::motorDuty1 , 0);
	writeToSM( shm, SM::QR::motorDuty2 , 0);
	writeToSM( shm, SM::QR::motorDuty3 , 0);
	writeToSM( shm, SM::QR::motorDuty4 , 0);
	//Initialize System States
	writeToSM( shm, SM::systemFailure , 0);
	writeToSM( shm, SM::platformType , DEFS::PLATFORM::QUADROTOR);	// Use quadrotor as primary platform
	writeToSM( shm, SM::sysStatus , DEFS::STATES::RUNNING);			// Use quadrotor as primary platform

}

int main(int argc, char *argv[])
{
	//Initialize Shared Memory
	initSharedMemory();

	//Parse Program Arguments with Getopt
    int aflag = 0;
    int bflag = 0, tflag = 0;
    char *tvalue = NULL;
    int index;
    int c;

    opterr = 0;

    while ((c = getopt (argc, argv, "abt:")) != -1)
      switch (c){
        case 'a':
          aflag = 1;
          break;
        case 'b':
          bflag = 1;
          break;
        case 't':
        	tvalue = optarg;
        	tflag = 1;
          break;
        case '?':
          if (optopt == 't'){
            fprintf (stderr, "Option -%c requires an argument.\n", optopt);
          }
          else if (isprint (optopt))
            fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else{
            fprintf (stderr,
                     "Unknown option character `\\x%x'.\n",
                     optopt);
          }
          return 1;
        default: return 0;
      }

    //printf ("aflag = %d, bflag = %d, tvalue = %s\n", aflag, bflag, tvalue);
    if (tflag){
    	if ( strcmp(tvalue,"adc") == 0 ){
    		// Open ADC Testing
			printf("Test mode activated\n");
			writeToSM(shm, SM::adcTest, (char)1);
			int iret1 = pthread_create( &adcThread, NULL, adcThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("ADC Poll.\n");
			}
    	}
    	if ( strcmp(tvalue,"serial1") == 0 ){
    		// Open Serial1 Testing
			printf("Test mode activated\n");
			writeToSM(shm, SM::serial1Test, (char)1);
			int iret2 = pthread_create( &serial1Thread, NULL, serial1ThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("Serial1 Poll.\n");
			}
    	}
    }
    else{
    	//Main System Loop
    	initThreads();
    	main_loop();

    }

}

