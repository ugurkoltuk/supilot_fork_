/*
 ============================================================================
 Name        : su_autopilot.h
 Author      : Taygun Kekec
 Version     : 1.0
 Date		 : March 2013
 Copyright   : GNU
 Description :
			   This file covers entry point of MonitorApp application
			   Using MonitorApp application users can:

			   -Show shared memory contents
			   -Send basic commands to the core module
			   -Show some basic log content
 ============================================================================
 */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "suShmLib.h"

char *shm;							// Shared Memory instance
int shmid;							// Shared Memory ID
key_t key;							// Shared Memory Key

int main(int argc, char *argv[])
{
    int shmid;
    key_t key;
	void *securityPtr;

    /* Get the segment named "5678", created by the server. */
	key = SHARED_MEMORY_KEY;

    /* Locate the segment */
    if ((shmid = shmget(key, SHARED_MEMORY_SIZE, 0666)) < 0) {
        perror("shmget");
        return -1;
    }

    /* Attach the segment to our data space. */
    if ((securityPtr = shmat(shmid, NULL, 0)) == (char *) -1) {
        perror("shmat");
        return -1;
    }
	shm = (char*)securityPtr;

    /* Now read what the server put in the memory. */
	int i;
    for (i = 0; i < SHARED_MEMORY_SIZE; i++){
		if ( shm[i] != SHARED_MEMORY_INIT_VAL ){
			perror("Error initializing shared memory");
			break;
		}
	}

    // Example access using pointer notation ( Variables will be updated, but using this is prone to errors
    char *sysFail = (char*) getSM(shm, SM::systemFailure);
    char *adcTest = (char*) getSM(shm, SM::adcTest);
    char *serial1Test = (char*) getSM(shm, SM::serial1Test);

    float *roll =  (float*) getSM(shm, SM::QR::roll);
    float *pitch =  (float*) getSM(shm, SM::QR::pitch);
    float *yaw =  (float*) getSM(shm, SM::QR::yaw);

    int *md1 = (int*) getSM(shm, SM::QR::motorDuty1);
    int *u1 = (int*) getSM(shm, SM::QR::motorDuty1);
    int *u2 = (int*) getSM(shm, SM::QR::motorDuty1);


    while ( 1 == 1){
    	printf("Roll : %f ", *roll);
        // Example access, copying from shared memory to local variable. This is less prone to errors but consumes more resources
    	// Use this if you are going to make changes on the variable
    	float roll_slowAccess = *(float*) getSM(shm, SM::QR::roll);
    	// Another access type
    	//printf("Roll : %f ", roll_slowAccess);

    	printf("Pitch : %f ", *pitch);
    	printf("Yaw : %f ", *yaw);
    	printf("U1 : %d ", *u1);
    	printf("U2 : %d ", *u2);
    	printf("M1 : %d ", *md1);
    	printf("adc1 : %f ", *(float*) getSM(shm, SM::QR::ADC1));
    	printf("systemFailure : %d ", *sysFail);
    	printf("adcTest : %d ", *adcTest);
    	printf("serial1Test : %d ", *serial1Test);
    	printf("\n");
    	usleep(250000);
    }

    //printf("Shared Memory Succesfully Initted\n");

    return 0;
}



